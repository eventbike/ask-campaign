module main

import os
import sqlite
import vweb
import setup

const (
	database_filename = 'campaign.db'
)

const (
	port = 9080
)

pub struct Campaign {
	vweb.Context
pub mut:
	db        sqlite.DB
	html_path vweb.RawHtml
	config    AppConfig
}

fn main() {
	vweb.run(load_app(), port)
}

fn load_app() &Campaign {
	if !os.exists(database_filename) {
		setup.bootstrap(database_filename) or { panic(err) }
	}
	mut app := &Campaign{
		db: sqlite.connect(database_filename) or { panic(err) }
	}
	app.handle_static('static', false)
	app.fill_config() // VTV not global any more
	return app
}

pub fn (mut app Campaign) before_request() {
	// VTV, this should not be generated on each request, but the V features for this are apparently missing
	app.fill_config()
}



fn list_names(list PartialUserList) string {
	mut returnstring := ''
	for i in 0 .. list.users.len {
		returnstring = '$returnstring${list.users[i]}'
		if i == (list.users.len - 2) {
			returnstring = '$returnstring und '
		} else if i < list.users.len - 2 {
			returnstring = '$returnstring, '
		}
	}
	return returnstring
}

fn limit(input string, limit int) string {
	return if input.len > limit { input.limit(limit) + '...' } else { input }
}

// Recipient

// VTV - switch back to optional once this can be handled in templates
fn (app Campaign) get_recipient(id int) /*?*/Recipient {
	recipient := sql app.db {
		select from Recipient where id == id
	}
	if recipient.id == id {
		return recipient
	} else {
		return Recipient{}//error('Question not found')
	}
}
