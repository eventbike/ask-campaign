module main

import vweb

pub fn (mut app Campaign) error_404(reason string) vweb.Result {
	app.set_status(404, 'Not found')
	return $vweb.html()
}

pub fn (mut app Campaign) error_500(reason string) vweb.Result {
	app.set_status(500, 'Internal server error')
	return $vweb.html()
}
