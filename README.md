# Some campaigning software without a proper name (yet)

This campaigning tool was born out of necessity for a spontaneous campaign of the Fridays for Future group in Potsdam.
They also act as a first testing of the concept - which is a little bit older and intended as a proper software project.

## The current status

This project is currently a dirty and minimal implementation in V which is an experimental language itself.
It allows to start the software anywhere (it creates an sqlite database), users can ask questions and support existing ones.
When compiling with [vlang.io](https://vlang.io), you only need to ship the binary file and the css included in the static folder.

## The idea

The most common tool for political participation between elections is starting a petition.
But those are easily ignored and only offer a complex statement where many people can either agree (and sign) or don't.
There are also forms of individual participation, e.g. by writing letters to your representatives.
In Germany, there is a tool called [abgeordnetenwatch.de](https://abgeordnetenwatch.de) that faciliates this process,
but you still need to know **whom** to ask and you usually write **complex statements**. Nothing you do every day.
Still, I think that asking questions every day is normal - at least I do so. I'm reading an article about some state support for dirty technology.
And I'd like to shout out and ask every name mentioned there why they do support this. Well, this needs a tool and this is what this project *might* become.
The main idea behind asking questions instead of demanding something is that responsible people will need to justify their actions.
This service will only be useful when there is enough public pressure by releasing the answers (or unanswered questions), e.g. to the local press.

(some of) the goals are:
- **simplicity:**
  just go ahead and ask your question in the moment you want to know about something; this requires some focus on mobile usability
- **powerful campaigning:**
  even if users should see no barrier in using this tool, the quality of the campaigns should be high; this needs some further discussion, but I do have some sort of community-completion of questions, recipient data etc in mind
- **flexibility:**
  asking questions with this software should not be limited to certain actors like above-mentioned abgeordnetenwatch; it should not matter whether you want to ask your representative in parliament, your local bank or a big company

## Contributing

Contributions are *very welcome*. Feel free to reach out if you have any questions about the concept.
Even non-code contributions helping in the campaigning mangagement will help a lot pushing this project forward.

## Where this might lead (= Roadmap)

Well, no one knows where this might lead, but here are some ideas and the original concept below:

### Minimum Testing (current status)

This software is used to fire a one-time local campaign for sustainable city development in Potsdam, Germany and detect some flaws.
These features are planned or already implemented:
- [x] ask a question and browse them
- [x] add yourself as supporter to a question
- [ ] improve listing of open questions
- [ ] provide information and facts about a topic (called "spices")
- [ ] improve code and logic for easier continued development
- [x] displaying replies to the questions, marking questions as "answered"
- [ ] eventually implement admin control, i.e. export of questions and users for mailing them

### General purpose campaigning

After the first test, the software should be prepared for public use so that other people can run their own campaigns. Therefore, the following is needed:
- [ ] allowing complete user customization
- [ ] setup wizard assisting in configuring the instance
- [ ] admin control over open questions and recipient information etc
- [ ] eventually spam protection etc
- [ ] community-completing information, e.g. adding a recipients mail address ...
- [ ] let users subscribe to questions and become informed about answers
- [ ] transparency stats: who answered (and how seriously // detailed), who did not?
- [ ] log IPs for spam protection
- [ ] questions close if all recipients reply

Things that need to be changed:
- [ ] allow replies without listing every recipient (at least if you want to continue supprting cases like "ask every party")
  probably via recipients and a group mapping (looking up if a recipient belongs to a group of recipients)
- [ ] probably better set recipient in user list and only link them
- [ ] use time stamps to allow to detect recent questions (instead of relying on id's)

### Starting a big platform

Whether this is desirable has to be evaluated after the previous phase, but instead of individual actors running their own instace, it could be desirable to have a general-purpose
platform where people can go ahead and ask their questions (similar to petition services).
Therefore, approx. the following features would be needed:
- [ ] scalability of the database and infrastructure
- [ ] sorting questions into categories to better understand where they should be redirected to


