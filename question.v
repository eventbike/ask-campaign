module main

import vweb

struct PartialUserList {
	users          []string
	absolute_count int
}

struct QuestionSummary {
	supporters PartialUserList
	recipients PartialUserList
mut:
	replies    []Reply
	question   Question
}

struct Recipient {
	id       int    [sql: serial]
	name     string
	email    string
	proposed int
}

struct User {
	id int      [sql: serial]
mut:
	name string
}

struct Question {
	id        int    [sql: serial]
	question  string
	reference string
mut:
	is_closed bool
}

struct Support {
	id          int [sql: serial]
	user_id     int
	question_id int
}

['/question/new']
pub fn (mut app Campaign) question_form() vweb.Result {
	proposed_recipients := sql app.db {
		select from Recipient where proposed > 0
	}
	app.add_header("Cache-Control","max-age=21600")
	return $vweb.html()
}

['/question/browse']
pub fn (mut app Campaign) question_browse() vweb.Result {
	newest_questions := app.get_newest_questions(3)
	random_questions := app.get_random_questions(5)
	random_replied_questions := app.get_random_replied_questions(5)
	return $vweb.html()
}

// VTV
['/question/get/random-questions']
pub fn (mut app Campaign) question_raw/*_random*/() vweb.Result {
	/*random_*/questions := app.get_random_questions(8)
	return $vweb.html()
	//return app.question_raw(random_questions[0])
}

/*
['/question/get/random-replied-questions']
pub fn (mut app Campaign) question_raw_replied() vweb.Result {
	replied_questions := app.get_random_replied_questions(5)
	return app.question_raw(replied_questions[0])
}

pub fn (mut app Campaign) question_raw(questions []QuestionSummary) vweb.Result {
	return $vweb.html()
}
*/

['/question/show/:question_id']
pub fn (mut app Campaign) question_show(question_id int) vweb.Result {
	return app.question_view(question_id, '')
}

['/question/thanks/:question_id']
pub fn (mut app Campaign) question_thanks(question_id int) vweb.Result {
	return app.question_view(question_id, 'Vielen Dank für deine Frage')
}

['/question/support/:question_id']
pub fn (mut app Campaign) question_support(question_id int) vweb.Result {
	return app.question_view(question_id, 'Vielen Dank für deine Unterstützung!')
}

['/question/share/:question_id']
pub fn (mut app Campaign) question_share(question_id int) vweb.Result {
	return app.question_view(question_id, app.config.share_motivation)
}

// VTV
pub fn (mut app Campaign) question_view(question_id int, custom_message string) vweb.Result {
	mut question_workaround := app.get_question_summary(question_id) or {
		return app.error_404(err.msg)
	}
	if custom_message != "" {
		question_workaround.question.is_closed = true
	}
	question := question_workaround
	return $vweb.html()
}

['/question/support/:id'; post]
pub fn (mut app Campaign) question_add_support(id int) vweb.Result {
	user_id := app.add_user(app.form['supporter_name'])
	app.add_support(user_id, id)
	return app.redirect('/question/support/$id')
}

// ToDo before release: escape inputs!
['/question/submit'; post]
pub fn (mut app Campaign) question_submit() vweb.Result {
	user_id := app.add_user(app.form['submitter_name'])
	recipient_id := app.get_recipient_id(app.form['recipient']) or {
		app.add_recipient(app.form['recipient'])
	}
	question_id := app.add_question(Question{
		question: app.form['question']
		reference: app.form['reference']
	})
	app.add_support(user_id, question_id)
	app.prepare_reply(question_id, recipient_id)
	return app.redirect('/question/thanks/$question_id')
}

// Reply

fn (mut app Campaign) prepare_reply(question_id int, recipient_id int) {
	empty_reply := Reply{
		question_id: question_id
		recipient_id: recipient_id
	}
	sql app.db {
		insert empty_reply into Reply
	}
}

fn (app Campaign) get_valid_replies(question_id int) []Reply {
	return sql app.db {
		select from Reply where reply > "" && question_id == question_id
	}
}

// Support

fn (app Campaign) add_support(user_id int, question_id int) {
	// ToDo: Check if question is already closed
	new_supporter := Support{
		question_id: question_id
		user_id: user_id
	}
	sql app.db {
		insert new_supporter into Support
	}
}

fn (app Campaign) get_supporters(question_id int, limit int) PartialUserList {
	absolute_supporter_count := sql app.db {
		select count from Support where question_id == question_id
	}
	random_supporter_ids, _ := app.db.exec('select user_id from support where question_id == $question_id order by random() limit $limit')
	mut supporter_names := []string{cap: limit}
	for supporter in random_supporter_ids {
		// VTV
		supporter_id := supporter.vals[0].int()
		user := sql app.db {
			select from User where id == supporter_id
		}
		supporter_names << user.name
	}
	return PartialUserList{
		users: supporter_names
		absolute_count: absolute_supporter_count
	}
}

// Question

fn (app Campaign) add_question(question Question) int {
	sql app.db {
		insert question into Question
	}
	return app.db.q_int('SELECT last_insert_rowid()')
}

fn (app Campaign) get_question_summary(question_id int) ?QuestionSummary {
	// VTV
	question_workaround := app.get_question(question_id) or { return error('Frage nicht gefunden') }
	question := question_workaround
	return QuestionSummary{
		question: question
		supporters: app.get_supporters(question_id, 3)
		recipients: app.get_recipients(question_id, 3)
		replies: app.get_valid_replies(question_id)
	}
}

fn (app Campaign) get_newest_questions(limit int) []QuestionSummary {
	newest_question_ids, _ := app.db.exec('select id from question order by id desc limit $limit')
	mut newest_questions := []QuestionSummary{cap: limit}
	for i in newest_question_ids {
		newest_questions << app.get_question_summary(i.vals[0].int()) or { break }
	}
	return newest_questions
}

fn (app Campaign) get_random_questions(limit int) []QuestionSummary {
	random_question_ids, _ := app.db.exec('select id from question where is_closed = 0 order by random() limit $limit')
	mut random_questions := []QuestionSummary{cap: limit}
	for i in random_question_ids {
		random_questions << app.get_question_summary(i.vals[0].int()) or { break }
	}
	return random_questions
}

fn (app Campaign) get_random_replied_questions(limit int) []QuestionSummary {
	random_replied_question_ids, _ := app.db.exec('select question_id from reply where reply != "" order by random() limit ${limit*2}')
	mut random_replied_questions := []QuestionSummary{cap: limit}
	for i in 0 .. random_replied_question_ids.len {
		if !(random_replied_question_ids[i] in random_replied_question_ids[..i]) {
			random_replied_questions << app.get_question_summary(random_replied_question_ids[i].vals[0].int()) or { continue }
		}
		if random_replied_questions.len > limit { break }
	}
	return random_replied_questions
}

fn (app Campaign) get_question(id int) ?Question {
	question := sql app.db {
		select from Question where id == id
	}
	if question.id == id {
		return question
	} else {
		return error('Question not found')
	}
}

// Recipient 

fn (app Campaign) get_recipient_id(name string) ?int {
	recipient_query := sql app.db {
		select from Recipient where name == name
	}
	if recipient_query.len > 0 {
		return recipient_query[0].id
	} else {
		return error('Recipient not found')
	}
}

fn (app Campaign) get_recipients(question_id int, limit int) PartialUserList {
	absolute_recipient_count := sql app.db {
		select count from Reply where question_id == question_id && is_hidden == 0
	}
	replies := sql app.db {
		select from Reply where question_id == question_id && is_hidden == 0 limit limit
	}
	mut recipient_names := []string{cap: limit}
	for reply in replies {
		// VTV
		recipient := sql app.db {
			select from Recipient where id == reply.recipient_id
		}
		recipient_names << recipient.name
	}
	return PartialUserList{
		users: recipient_names
		absolute_count: absolute_recipient_count
	}
}

fn (app Campaign) add_recipient(name string) int {
	new_recipient := Recipient{
		name: name
	}
	sql app.db {
		insert new_recipient into Recipient
	}
	return app.db.q_int('SELECT last_insert_rowid()')
}

// User

fn (app Campaign) add_user(name string) int {
	new_user := User{
		name: name
	}
	sql app.db {
		insert new_user into User
	}
	return app.db.q_int('SELECT last_insert_rowid()')
}
