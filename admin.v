module main

import vweb
import rand

// temporary workaround only!
['/admin/manual_prepare_reply']
pub fn (mut app Campaign) admin_manualpreparereply() vweb.Result {
	next_recipient_id := app.db.q_int('select recipient_id from reply where key = "" limit 1')
	recipient := app.get_recipient(next_recipient_id)
	all_replies_by_recipient := sql app.db {
		select from Reply where recipient_id == recipient.id
	}

	// give each reply a key and create list of questions to the recipient
	mut questions_to_recipient := []QuestionSummary{cap: all_replies_by_recipient.len}
	for reply in all_replies_by_recipient.filter(it.key == '') {
		mut question := app.get_question_summary(reply.question_id) or { continue }
		question.replies = [reply]
		// VTV: struct fields apparently dont work in ORM
		key := rand.string_from_set('abcdefghijklmnopqrstuvwxyz1234567890_-', 8) // VTV: vweb doesn't pass uppercase letters at the moment
		question.replies[0].key = key
		reply_id := reply.id
		sql app.db {
			update Reply set key = key where id == reply_id
		}
		questions_to_recipient << question
	}
	mail_text := app.mail_gen_reply_request(recipient, questions_to_recipient)
	return $vweb.html()
}
