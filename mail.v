module main

fn (app Campaign) mail_gen_reply_request(recipient Recipient, questions_to_recipient []QuestionSummary) string {
	return $tmpl('templates/mail/reply_request.txt') // VTV https://github.com/vlang/v/issues/9838 - remove hardcoded string later
}
