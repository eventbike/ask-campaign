module main

struct AppConfig {
mut:
	title                        string = 'Default App title'
	description                  string = 'You can describe your campaign here ...'
	privacyurl                   string
	impressurl                   string
	app_url                      string = 'http://localhost'
	share_label                  string = 'Share question'
	share_motivation             string = 'You can do a lot to this question by sharing it!'
	mail_replyrequest_salutation string = 'Hello {{Recipient}},'
	mail_replyrequeset_intro     string = 'You received a question via our campaigning platform ...\n\nexpand this explanation ...'
	mail_replyrequest_asking     string = 'You are asked by {{Who}}:'
	mail_replyrequest_answer     string = 'You can answer this question by clicking this link: {{Link}}'
	mail_replyrequest_outro      string = 'Some more information at the end of the mail'
}

struct Config {
	id    int    [sql: serial]
	key   string
	value string
}

fn (app Campaign) get_config(queried_key string) ?string {
	value := sql app.db {
		select from Config where key == queried_key
	}
	return if value.len > 0 {
		value[0].value
	} else {
		eprintln('Missing config: $queried_key')
		error('Missing config: $queried_key')
	}
}

fn (mut app Campaign) fill_config() {
	$for field in AppConfig.fields {
		// VTV - doesn't work without this check for some reason
		$if field.typ is string {
			app.config.$(field.name) = app.get_config(field.name) or { app.config.$(field.name) }
		}
	}
}
