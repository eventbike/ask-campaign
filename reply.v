module main

import vweb

struct Reply {
	id           int    [sql: serial]
	question_id  int
	recipient_id int
	reply        string
	is_hidden    int
mut:
	key string
}

['/reply/edit/:reply_id/:key']
pub fn (mut app Campaign) reply_edit(reply_id int, key string) vweb.Result {
	if key == '' || reply_id == 0 {
		return app.error_404('Keine Antwort gefunden.')
	}
	reply_list := sql app.db {
		select from Reply where id == reply_id && key == key
	}
	if reply_list.len != 1 {
		return app.error_500('Missing error handling') // ToDo: handle wrong key as 403 instead
	}
	reply := reply_list[0]
	if reply.question_id == 0 {
		return app.error_404('')
	}
	// VTV - directly working on question breaks the template for some reason
	mut question_workaround := app.get_question_summary(reply.question_id) or {
		return app.error_500('')
	}
	question_workaround.question.is_closed = true
	question := question_workaround
	custom_message := app.query['message']
	return $vweb.html()
}

['/reply/edit/:reply_id/:key'; post]
pub fn (mut app Campaign) reply_edit_submit(reply_id int, key string) vweb.Result {
	if key == '' || reply_id == 0 {
		return app.error_500('Missing error handling')
		// app.redirect('/reply/edit/$reply_id/$key?message=error')
	}
	replytext := app.form['reply']
	sql app.db {
		update Reply set reply = replytext where id == reply_id && key == key
	}
	return app.redirect('/reply/edit/$reply_id/$key')
}
