module setup

import os
import sqlite

pub fn bootstrap(filename string) ? {
	os.create(filename) or { return err }
	mut db := sqlite.connect(filename) or { return err }
	init_database(db) or { return err }
	db.close() or { return err }
}

pub fn init_database(db sqlite.DB) ? {
	file := $embed_file('assets/database.sql')
	statements_array := file.to_string().split(';')
	for statement in statements_array {
		if statement != '\n' {
			if db.exec_none(statement) != 101 {
				return error('Query failed: $statement')
			}
		}
	}
}
