module main

import vweb

pub fn (mut app Campaign) index() vweb.Result {
	return app.home_index()
}

pub fn (mut app Campaign) home_index() vweb.Result {
	custom_message := ''
	question := app.get_question_summary(app.db.q_int('select id from question where is_closed = 0 order by random() limit 1')) or {
		QuestionSummary{}
	}
	return $vweb.html()
}
