BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "user" (
	"id"	INTEGER,
	"name"	TEXT,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "config" (
	"id"	INTEGER,
	"key"	INTEGER UNIQUE,
	"value"	TEXT,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "spice" (
	"id"	INTEGER,
	"title"	TEXT,
	"reference"	TEXT,
	"spice"	TEXT,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "recipient" (
	"id"	INTEGER,
	"name"	TEXT,
	"email"	TEXT,
	"proposed"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "question" (
	"id"	INTEGER,
	"question"	TEXT,
	"reference"	TEXT,
	"recipient_id"	INTEGER,
	"is_closed"	INTEGER NOT NULL,
	"time"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("recipient_id") REFERENCES "recipient"("id")
);
CREATE TABLE IF NOT EXISTS "support" (
	"user_id"	INTEGER,
	"question_id"	INTEGER,
	"id"	INTEGER,
	"remote_address"	TEXT,
	FOREIGN KEY("user_id") REFERENCES "user"("id"),
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("question_id") REFERENCES "question"("id")
);
CREATE TABLE IF NOT EXISTS "reply" (
	"id"	INTEGER,
	"question_id"	INTEGER,
	"recipient_id"	INTEGER,
	"reply"	TEXT,
	"key"	TEXT,
	"is_hidden"	INTEGER,
	"edit_time"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("recipient_id") REFERENCES "recipient"("id"),
	FOREIGN KEY("question_id") REFERENCES "question"("id")
);
COMMIT;
